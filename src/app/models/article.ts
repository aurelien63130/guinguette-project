export class Article {
  titre: string;
  contenu: string;
  image: string;

  constructor(titre: string, contenu: string, image: string) {
    this.titre = titre;
    this.contenu = contenu;
    this.image = image;
  }
}
