import {Component, Input, OnInit} from '@angular/core';
import {Article} from "../../models/article";

@Component({
  selector: 'app-guinguette',
  templateUrl: './guinguette.component.html',
  styleUrls: ['./guinguette.component.css']
})
export class GuinguetteComponent implements OnInit {
  @Input() articleFavori !: Article;
  constructor() { }

  ngOnInit(): void {
  }

}
