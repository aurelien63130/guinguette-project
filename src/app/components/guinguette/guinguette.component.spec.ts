import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuinguetteComponent } from './guinguette.component';

describe('GuinguetteComponent', () => {
  let component: GuinguetteComponent;
  let fixture: ComponentFixture<GuinguetteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuinguetteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuinguetteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
