import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Article} from "../../models/article";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {

  @Input() article: Article = new Article("Hello", "world", "guinguette.jpg");
  @Output() voteEmitter: EventEmitter<Article> = new EventEmitter<Article>();

  constructor(private toastrService: ToastrService) { }

  ngOnInit(): void {
  }

  vote(): void {
    this.voteEmitter.emit(this.article);
    this.toastrService.info("Félicitations !", "Vote pris en compte",
      {
      timeOut: 3000,
    })
  }

}
