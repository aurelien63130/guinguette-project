import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-for',
  templateUrl: './for.component.html',
  styleUrls: ['./for.component.css']
})
export class ForComponent implements OnInit {

  tableau = ["Aurélien", "Pierrick", "Greg", "Audrey", "Gaetan", "Aurore","..."];
  numbers !: number[];

  constructor() { }

  ngOnInit(): void {
    this.numbers = Array(100).fill(0).map((x,i)=>i);
    console.log(this.numbers);
  }

}
