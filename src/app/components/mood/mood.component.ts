import { Component, OnInit } from '@angular/core';
import { faSmile, faFaceFrownOpen, faAmbulance } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-mood',
  templateUrl: './mood.component.html',
  styleUrls: ['./mood.component.css']
})
export class MoodComponent implements OnInit {

  mood !: string;
  faSmile = faSmile;
  faFaceFrownOpen = faFaceFrownOpen;
  faAmbulance = faAmbulance;

  constructor() { }

  ngOnInit(): void {
  }

  changeMood(mood: string) {
    this.mood = mood;
  }



}
