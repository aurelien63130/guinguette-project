import { Component, OnInit } from '@angular/core';
import { faSun, faMoon } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-if',
  templateUrl: './if.component.html',
  styleUrls: ['./if.component.css']
})
export class IfComponent implements OnInit {
  faSun = faSun;
  faMoon = faMoon;

  activeSelector = 'sun';

  constructor() { }

  ngOnInit(): void {
  }

  changeTheme(newSelector: string): void {
    this.activeSelector = newSelector;
  }

}
