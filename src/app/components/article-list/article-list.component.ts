import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from "../../models/article";

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  articles: Article[] = [];
  @Output() vote: EventEmitter<Article> = new EventEmitter<Article>();

  constructor() { }

  ngOnInit(): void {
    this.articles = [
      new Article("Avec Yohan !", "En cours de BDD !", "img_1.jpg"),
      new Article("Encore avec Yohan !", "Toujours en BDD", "img_2.jpg"),
      new Article("Avec Pierrick !", "In English lesson", 'img_3.jpg')
    ]
  }

  voteDetail($event: Article):void {
    this.vote.emit($event);
  }

}
