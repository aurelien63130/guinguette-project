import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderMenuComponent } from './components/header-menu/header-menu.component';
import { GuinguetteComponent } from './components/guinguette/guinguette.component';
import { ArticleListComponent } from './components/article-list/article-list.component';
import { ArticleDetailComponent } from './components/article-detail/article-detail.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
import { MoodComponent } from './components/mood/mood.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ForComponent } from './components/for/for.component';
import { IfComponent } from './components/if/if.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderMenuComponent,
    GuinguetteComponent,
    ArticleListComponent,
    ArticleDetailComponent,
    MoodComponent,
    ForComponent,
    IfComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
